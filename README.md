# OpenGL Spaceship Render
<br>Final project for PROG54310 Computer Graphics and Animation
<br>It contains a rotating spaceship and a light source.
<br>The control panel can change the lighting from the light source.
<br>The code only runs at x86 mode. 
<br>Spaceship model and textures are Sheridan course materials. 
<br>
<br>![Screenshot](/images/Screenshot.png)

#include "GameController.h"
#include "WindowController.h"
#include "ToolWindow.h"
#include "Fonts.h"

#include "AseLoader.h"

glm::vec3 GameController::lightTarget = glm::vec3(0, 0, 0);
double GameController::xpos = 0;
double GameController::ypos = 0;
string GameController::leftButtonStat = "UP";
string GameController::middleButtonStat = "UP";
int GameController::moving = 0;

GameController::GameController()
{
	m_shaderColor = { };
	m_shaderDiffuse = {};
	m_camera = { };
	m_meshBoxes.clear();
}

GameController :: ~GameController()
{

}


void GameController::Initialize()
{
	

	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	//const GLubyte* version = glGetString(GL_VERSION);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	srand(time(0));
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	m_camera = Camera(WindowController::GetInstance().GetResolution());
}

//void GameController::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
//{
//	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
//	{
//		glfwGetCursorPos(window, &xpos, &ypos);
//		glm::vec3 curPos = Mesh::Lights[count].GetPosition();
//		lightTarget = curPos+glm::vec3(xpos/100,ypos/100,0);
//	}
//}

void GameController::RunGame()
{
	OpenGL::ToolWindow^ window = gcnew OpenGL::ToolWindow();
	window->Show();



	m_shaderColor = Shader();
	m_shaderColor.LoadShaders("Color.vert", "Color.frag");

	m_shaderDiffuse = Shader();
	m_shaderDiffuse.LoadShaders("Diffuse.vert", "Diffuse.frag");

	m_shaderFont = Shader();
	m_shaderFont.LoadShaders("Font.vert", "Font.frag");

	m_shaderSkybox = Shader();
	m_shaderSkybox.LoadShaders("Skybox.vert", "Skybox.frag");

#pragma region CreateMeshes

	Mesh m = Mesh();
	m.CreateAse(&m_shaderColor, "../Assets/Models/Ball.ase");
	m.SetColor({ 1.0f,1.0f,1.0f });
	m.SetScale({ 0.01f,0.01f,0.01f });
	m.SetPosition({ 0.0f,0.0f,3.0f });
	Mesh::Lights.push_back(m);

	Mesh fighter = Mesh();
	fighter.CreateAse(&m_shaderDiffuse, "../Assets/Models/Fighter.ase");
	fighter.SetCameraPosition(m_camera.GetPosition());
	fighter.SetScale({ 0.0025f,0.0025f,0.0025f });
	fighter.SetPosition({ 0.0f,0.0f,0.0f });
	m_meshBoxes.push_back(fighter);

	//Mesh fighter1 = Mesh();
	//fighter1.Create(&m_shaderDiffuse, "../Assets/Models/Fighter.obj");
	//fighter1.SetCameraPosition(m_camera.GetPosition());
	//fighter1.SetScale({ 0.0025f,0.0025f,0.0025f });
	//fighter1.SetPosition({ 0.0f,0.0f,0.0f });
	//m_meshBoxes.push_back(fighter1);

	//Mesh asteroid = Mesh();
	//asteroid.CreateAse(&m_shaderDiffuse, "../Assets/Models/Asteroid1.ase");
	//asteroid.SetCameraPosition(m_camera.GetPosition());
	//asteroid.SetScale({ 0.05f,0.05f,0.05f });
	//asteroid.SetPosition({ 2.0f,0.0f,0.0f });
	//m_meshBoxes.push_back(asteroid);

#pragma endregion CreateMeshes

	Fonts f = Fonts();
	f.Create(&m_shaderFont, "arial.ttf", 100);

	double lastTime = glfwGetTime();
	int fps = 0;
	string fpsS = "0";
	string mousePos = "0,0";
	string leftButton = "";
	string middleButton = "";
	string fighterPos = "";
	string fighterRot = "";
	string fighterScale = "";
	GLFWwindow* win = WindowController::GetInstance().GetWindow();

	glfwSetMouseButtonCallback(win, GameController::mouse_button_callback);

	do
	{
		System::Windows::Forms::Application::DoEvents();
		glm::vec3 specularColor = glm::vec3(OpenGL::ToolWindow::R, OpenGL::ToolWindow::G, OpenGL::ToolWindow::B);
		glfwGetCursorPos(win, &xpos, &ypos);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		double currentTime = glfwGetTime();
		fps++;
		if (currentTime - lastTime >= 1.0)
		{
			fpsS = "FPS: " + to_string(fps);
			fps = 0;
			lastTime += 1.0f;
		}
		glm::vec2 scale = glm::vec2(0.2, 0.25);
		mousePos = "Mouse Pos: " + to_string(xpos) + " " + to_string(ypos);
		leftButton = "Left Button: " + leftButtonStat;
		middleButton = "Middle Button: " + middleButtonStat;

		f.RenderText(fpsS, 100, 100, scale, { 1.0f,1.0f,1.0f });
		f.RenderText(mousePos, 100, 150, scale, { 1.0f,1.0f,1.0f });
		f.RenderText(leftButton, 100, 200, scale, { 1.0f,1.0f,1.0f });
		f.RenderText(middleButton, 100, 250, scale, { 1.0f,1.0f,1.0f });

		for (unsigned int count = 0; count < m_meshBoxes.size(); count++)
		{
			m_meshBoxes[count].SetSpecularStrength(OpenGL::ToolWindow::specularStrength);
			m_meshBoxes[count].SetSpecularColor(specularColor);
			m_meshBoxes[count].Render(m_camera.GetProjection() * m_camera.GetView());
		}

		if (window->moveLight)
		{
			for (unsigned int count = 0; count < Mesh::Lights.size(); count++)
			{
				if (moving > 0 && !window->resetLightPos)
				{
					Mesh::Lights[count].SetPosition(Mesh::Lights[count].GetPosition() + glm::vec3((xpos - 683) / (683 * 6), -(ypos - 384) / (384 * 6), 0));
					moving--;
				}
				if (window->resetLightPos)
				{
					Mesh::Lights[count].SetPosition(glm::vec3(0,0,3));
					window->resetLightPos = false;
				}
				Mesh::Lights[count].Render(m_camera.GetProjection() * m_camera.GetView());
			}
		}
		fighterPos = "Fighter Position: " + to_string(m_meshBoxes[0].GetPosition());
		fighterRot = "Fighter Rotation: " + to_string(m_meshBoxes[0].GetRotation());
		fighterScale = "Fighter Scale: " + to_string(m_meshBoxes[0].GetScale());
		f.RenderText(fighterPos, 100, 300, scale, { 1.0f,1.0f,1.0f });
		f.RenderText(fighterRot, 100, 350, scale, { 1.0f,1.0f,1.0f });
		f.RenderText(fighterScale, 100, 400, scale, { 1.0f,1.0f,1.0f });
		glfwSwapBuffers(win);
		glfwPollEvents();
	} while (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(win) == 0);

	for (unsigned int count = 0; count < m_meshBoxes.size(); count++)
	{
		m_meshBoxes[count].CleanUp();
	}
	for (unsigned int count = 0; count < Mesh::Lights.size(); count++)
	{
		Mesh::Lights[count].CleanUp();
	}
	//m_skybox.Cleanup();
	m_shaderDiffuse.CleanUp();
	m_shaderColor.CleanUp();
	m_shaderSkybox.CleanUp();
}

void GameController::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		leftButtonStat = "DOWN";
		moving = 30;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		leftButtonStat = "UP";
	}
	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
	{
		middleButtonStat = "DOWN";
	}
	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE)
	{
		middleButtonStat = "UP";
	}
}
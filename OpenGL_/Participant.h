#ifndef PARTICIPANT_H
#define PARTICIPANT_H

#include "StandardIncludes.h"
#include "Texture.h"
class Shader;

class Participant
{
public:
	//Constructors/Destructors
	Participant();
	virtual ~Participant();

	//Methods
	void Create(Shader* _shader);
	void CleanUp();
	void Render(glm::mat4 _wvp);

private:
	Shader* m_shader;
	Texture m_texture;
	Texture m_texture2;
	GLuint m_vertexBuffer;	//GPU buffer pointer
	GLuint m_indexBuffer;	//GPU buffer pointer
	vector<GLfloat> m_vertexData;
	vector<GLubyte> m_indexData;
	glm::vec3 m_position;
	glm::vec3 m_rotation;

};


#endif // !PARTICIPANT_H



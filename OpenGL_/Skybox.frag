#version 330

in vec3 retTexCoords;

out vec4 FragColor;

uniform samplerCube skyboxTexture;

void main()
{
	FragColor = texture(skyboxTexture,retTexCoords);
}
#include "AseLoader.h"
AseLoader::AseLoader()
{
	material_count = 0;
	num_meshvertex = 0;
	num_faces = 0;
	num_tvertex = 0;
	num_tvfaces = 0;
	material_ref = 0;

	diffuse_map.uvw_u_offset = 0;
	diffuse_map.uvw_v_offset = 0;
	diffuse_map.uvw_u_tiling = 0;
	diffuse_map.uvw_v_tiling = 0;

	specular_map.uvw_u_offset = 0;
	specular_map.uvw_v_offset = 0;
	specular_map.uvw_u_tiling = 0;
	specular_map.uvw_v_tiling = 0;

	normal_map.uvw_u_offset = 0;
	normal_map.uvw_v_offset = 0;
	normal_map.uvw_u_tiling = 0;
	normal_map.uvw_v_tiling = 0;
}
AseLoader::~AseLoader()
{
	LoadedMeshes.clear();
}

bool AseLoader::LoadFile(std::string Path)
{
	if (Path.substr(Path.size() - 4, 4) != ".ase")
		return false;


	std::ifstream file(Path);
	int* intHandle;
	glm::vec3* vec3fHandle;
	AseLoader::Map* mapHandle;
	if (!file.is_open())
		return false;

	LoadedMeshes.clear();

	std::string file_str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	//parse material_count
	intHandle = GetInt("*MATERIAL_COUNT", file_str);
	if (intHandle != nullptr)
	{
		material_count = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	//parse scene_ambient_static
	vec3fHandle = GetVec3f("*SCENE_AMBIENT_STATIC", file_str);
	if (vec3fHandle != nullptr)
	{
		scene_ambient_static = *vec3fHandle;
	}
	else
	{
		return false;
	}
	delete vec3fHandle;

	//parse material_diffuse
	vec3fHandle = GetVec3f("*MATERIAL_DIFFUSE", file_str);
	if (vec3fHandle != nullptr)
	{
		material_diffuse = *vec3fHandle;
	}
	else
	{
		return false;
	}
	delete vec3fHandle;

	//parse material_specular
	vec3fHandle = GetVec3f("*MATERIAL_SPECULAR", file_str);
	if (vec3fHandle != nullptr)
	{
		material_specular = *vec3fHandle;
	}
	else
	{
		return false;
	}
	delete vec3fHandle;

	//parse diffuse_map
	mapHandle = GetMap("*MAP_DIFFUSE", file_str);
	if (mapHandle != nullptr)
	{
		diffuse_map = *mapHandle;
	}
	delete mapHandle;

	//parse specular_map
	mapHandle = GetMap("*MAP_SPECULAR", file_str);
	if (mapHandle != nullptr)
	{
		specular_map = *mapHandle;
	}
	delete mapHandle;

	//parse normal_map
	mapHandle = GetMap("*MAP_BUMP", file_str);
	if (mapHandle != nullptr)
	{
		normal_map = *mapHandle;
	}
	delete mapHandle;

	//parse num_meshvertex
	intHandle = GetInt("*MESH_NUMVERTEX", file_str);
	if (intHandle != nullptr)
	{
		num_meshvertex = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	//parse num_faces
	intHandle = GetInt("*MESH_NUMFACES", file_str);
	if (intHandle != nullptr)
	{
		num_faces = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	//parse mesh_vertex_list
	FillVec3List("*MESH_VERTEX_LIST", "*MESH_VERTEX", file_str, mesh_vertex_list);

	//parse mesh_face_list
	FillFaceList("*MESH_FACE_LIST", "*MESH_FACE", file_str, mesh_face_list);

	//parse num_tvertex
	intHandle = GetInt("*MESH_NUMTVERTEX", file_str);
	if (intHandle != nullptr)
	{
		num_tvertex = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	//parse mesh_tvertlist
	FillVec3List("*MESH_TVERTLIST", "*MESH_TVERT", file_str, mesh_tvertlist);

	//parse num_tvfaces
	intHandle = GetInt("*MESH_NUMTVFACES", file_str);
	if (intHandle != nullptr)
	{
		num_tvfaces = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	//parse mesh_tfacelist
	FillVec3iList("*MESH_TFACELIST", "*MESH_TFACE", file_str, mesh_tfacelist);

	//parse mesh_normals
	FillNormalList(file_str, mesh_normals);

	//parse material_ref
	intHandle = GetInt("*MATERIAL_REF", file_str);
	if (intHandle != nullptr)
	{
		material_ref = *intHandle;
	}
	else
	{
		return false;
	}
	delete intHandle;

	Ase2Obj();

	return true;
}

int* AseLoader::GetInt(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string line_str;
	std::string value_str;
	int* value;
	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("\n", start);
		line_str = _file.substr(start, end - start + 1);
		start = line_str.find(" ");
		end = line_str.find("\n", start);
		value_str = line_str.substr(start, end - start);
		value = new int(std::stoi(value_str));
		return value;
	}
	return nullptr;
}

float* AseLoader::GetFloat(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string line_str;
	std::string value_str;
	float* value;
	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("\n", start + 1);
		line_str = _file.substr(start, end - start + 1);
		start = line_str.find(" ");
		end = line_str.find("\n", start + 1);
		value_str = line_str.substr(start, end - start);
		value = new float(std::stof(value_str));
		return value;
	}
	return nullptr;
}

std::string* AseLoader::GetString(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string line_str;
	std::string* value_str;

	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("\n", start + 1);
		line_str = _file.substr(start, end - start + 1);
		start = line_str.find(" ");
		end = line_str.find("\n", start + 1);
		value_str = new std::string(line_str.substr(start, end - start));
		return value_str;
	}
	return nullptr;
}

glm::vec3* AseLoader::GetVec3f(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string line_str;
	std::string value_str;
	glm::vec3* value = new glm::vec3();

	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("\n", start + 1);
		line_str = _file.substr(start, end - start + 1);
		//parse x
		start = line_str.find(" ");
		end = line_str.find("\t", start + 1);
		value_str = line_str.substr(start, end - start);
		value->x = stof(value_str);
		//parse y
		start = end;
		end = line_str.find("\t", start + 1);
		value_str = line_str.substr(start, end - start);
		value->y = stof(value_str);
		//parse z
		start = end;
		end = line_str.find("\n", start + 1);
		value_str = line_str.substr(start, end - start);
		value->z = stof(value_str);
		return value;
	}
	return nullptr;
}

glm::ivec3* AseLoader::GetVec3i(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string line_str;
	std::string value_str;
	glm::ivec3* value = new glm::ivec3();

	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("\n", start + 1);
		line_str = _file.substr(start, end - start + 1);
		//parse x
		start = line_str.find(" ");
		end = line_str.find("\t", start + 1);
		value_str = line_str.substr(start, end - start);
		value->x = stoi(value_str);
		//parse y
		start = end;
		end = line_str.find("\t", start + 1);
		value_str = line_str.substr(start, end - start);
		value->y = stoi(value_str);
		//parse z
		start = end;
		end = line_str.find("\n", start + 1);
		value_str = line_str.substr(start, end - start);
		value->z = stoi(value_str);

		return value;
	}
	return nullptr;
}

AseLoader::Map* AseLoader::GetMap(const char* _entry, const std::string& _file)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::string obj_str;
	AseLoader::Map* value = new AseLoader::Map();
	std::string* stringHandle;
	float* floatHandle;

	start = _file.find(_entry);
	if (start != std::string::npos)
	{
		end = _file.find("}", start);
		obj_str = _file.substr(start, end - start + 1);
		//parse bitmap
		stringHandle = GetString("*BITMAP", obj_str);
		if (stringHandle != nullptr)
		{
			value->bitmap = *stringHandle;
		}
		delete stringHandle;

		//parse uvw_u_offset
		floatHandle = GetFloat("*UVW_U_OFFSET", obj_str);
		if (floatHandle != nullptr)
		{
			value->uvw_u_offset = *floatHandle;
		}
		delete floatHandle;

		//parse uvw_v_offset
		floatHandle = GetFloat("*UVW_V_OFFSET", obj_str);
		if (floatHandle != nullptr)
		{
			value->uvw_v_offset = *floatHandle;
		}
		delete floatHandle;

		//parse uvw_u_tiling
		floatHandle = GetFloat("*UVW_U_TILING", obj_str);
		if (floatHandle != nullptr)
		{
			value->uvw_u_tiling = *floatHandle;
		}
		delete floatHandle;

		//parse uvw_v_tiling
		floatHandle = GetFloat("*UVW_V_TILING", obj_str);
		if (floatHandle != nullptr)
		{
			value->uvw_v_tiling = *floatHandle;
		}
		delete floatHandle;

		return value;
	}
	return nullptr;
}

void AseLoader::FillVec3iList(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::ivec3>& _targetList)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::size_t lineStart = 0;
	std::size_t lineEnd = 0;
	std::string obj_str;
	std::string value_str;

	start = _file.find("{", _file.find(_entry));
	if (start != std::string::npos)
	{
		end = _file.find("}", start);
		obj_str = _file.substr(start, end - start + 1);
		lineStart = obj_str.find(_elementEntry);
		while (lineStart != std::string::npos)
		{
			lineEnd = obj_str.find("\n", lineStart + 1);

			std::size_t indexPos = obj_str.find(" ", lineStart + 1);
			std::size_t xPos = obj_str.find("\t", indexPos + 1);
			std::size_t yPos = obj_str.find("\t", xPos + 1);
			std::size_t zPos = obj_str.find("\t", yPos + 1);
			value_str = obj_str.substr(indexPos, xPos - indexPos);
			int index = stoi(value_str);
			glm::ivec3 vector = glm::vec3();

			value_str = obj_str.substr(xPos, yPos - xPos);
			vector.x = stoi(value_str);

			value_str = obj_str.substr(yPos, zPos - yPos);
			vector.y = stoi(value_str);

			value_str = obj_str.substr(zPos, lineEnd - zPos);
			vector.z = stoi(value_str);

			_targetList.insert(std::pair<int, glm::ivec3>(index, vector));

			lineStart = obj_str.find(_elementEntry, lineEnd);
		}
	}

}

void AseLoader::FillVec3List(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::vec3>& _targetList)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::size_t lineStart = 0;
	std::size_t lineEnd = 0;
	std::string obj_str;
	std::string value_str;

	start = _file.find("{", _file.find(_entry));
	if (start != std::string::npos)
	{
		end = _file.find("}", start);
		obj_str = _file.substr(start, end - start + 1);
		lineStart = obj_str.find(_elementEntry);
		while (lineStart != std::string::npos)
		{
			lineEnd = obj_str.find("\n", lineStart + 1);

			std::size_t indexPos = obj_str.find(" ", lineStart + 1);
			std::size_t xPos = obj_str.find("\t", indexPos);
			std::size_t yPos = obj_str.find("\t", xPos + 1);
			std::size_t zPos = obj_str.find("\t", yPos + 1);
			value_str = obj_str.substr(indexPos, xPos - indexPos);
			int index = stoi(value_str);
			glm::vec3 vector = glm::vec3();

			value_str = obj_str.substr(xPos, yPos - xPos);
			vector.x = stof(value_str);

			value_str = obj_str.substr(yPos, zPos - yPos);
			vector.y = stof(value_str);

			value_str = obj_str.substr(zPos, lineEnd - zPos);
			vector.z = stof(value_str);

			_targetList.insert(std::pair<int, glm::vec3>(index, vector));

			lineStart = obj_str.find(_elementEntry, lineEnd);
		}
	}
}

void AseLoader::FillFaceList(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::ivec3>& _targetList)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::size_t lineStart = 0;
	std::size_t lineEnd = 0;
	std::string obj_str;
	std::string value_str;

	start = _file.find("{", _file.find(_entry));
	if (start != std::string::npos)
	{
		end = _file.find("}", start);
		obj_str = _file.substr(start, end - start + 1);
		lineStart = obj_str.find(_elementEntry);
		while (lineStart != std::string::npos)
		{
			lineEnd = obj_str.find("\n", lineStart + 1);

			std::size_t indexPos = obj_str.find(" ", lineStart + 1);
			std::size_t indexEnd = obj_str.find(":", indexPos + 1);
			std::size_t xPos = obj_str.find("A:", indexEnd + 1) + 2;
			std::size_t yPos = obj_str.find("B:", indexEnd + 1) + 2;
			std::size_t zPos = obj_str.find("C:", indexEnd + 1) + 2;
			value_str = obj_str.substr(indexPos, indexEnd - indexPos);
			int index = stoi(value_str);
			glm::ivec3 vector = glm::vec3();

			value_str = obj_str.substr(xPos, yPos - xPos);
			vector.x = stoi(value_str);

			value_str = obj_str.substr(yPos, zPos - yPos);
			vector.y = stoi(value_str);

			value_str = obj_str.substr(zPos, lineEnd - zPos);
			vector.z = stoi(value_str);

			_targetList.insert(std::pair<int, glm::ivec3>(index, vector));

			lineStart = obj_str.find(_elementEntry, lineEnd);
		}
	}
}

void AseLoader::FillNormalList(const std::string& _file, std::map<int, FaceNormal>& _targetList)
{
	std::size_t start = 0;
	std::size_t end = 0;
	std::size_t faceStart = 0;
	std::size_t faceEnd = 0;
	std::size_t lineStart = 0;
	std::size_t lineEnd = 0;
	std::string obj_str;
	std::string value_str;
	int faceIndex = 0;

	start = _file.find("{", _file.find("*MESH_NORMALS"));
	if (start != std::string::npos)
	{
		end = _file.find("}", start);
		obj_str = _file.substr(start, end - start + 1);
		faceStart = obj_str.find("*MESH_FACENORMAL");
		while (faceStart != std::string::npos)
		{
			std::size_t indexPos = obj_str.find(" ", faceStart + 1);
			std::size_t xPos = obj_str.find("\t", indexPos);
			value_str = obj_str.substr(indexPos, xPos - indexPos);
			faceIndex = stoi(value_str);

			lineEnd = faceStart;
			FaceNormal fn;
			for (int i = 0; i < 3; i++)
			{
				lineStart = obj_str.find("*MESH_VERTEXNORMAL", lineEnd);
				lineEnd = obj_str.find("\n", lineStart);

				std::size_t indexPos = obj_str.find(" ", lineStart + 1);
				std::size_t xPos = obj_str.find("\t", indexPos);
				std::size_t yPos = obj_str.find("\t", xPos + 1);
				std::size_t zPos = obj_str.find("\t", yPos + 1);
				glm::vec4 vector = glm::vec4();

				value_str = obj_str.substr(indexPos, xPos - indexPos);
				vector.w = stoi(value_str);
				
				value_str = obj_str.substr(xPos, yPos - xPos);
				vector.x = stof(value_str);

				value_str = obj_str.substr(yPos, zPos - yPos);
				vector.y = stof(value_str);

				value_str = obj_str.substr(zPos, lineEnd - zPos);
				vector.z = stof(value_str);

				fn.vertexNormal[i] = vector;
			}

			_targetList.insert(std::pair<int, FaceNormal>(faceIndex, fn));

			faceStart = obj_str.find("*MESH_FACENORMAL", faceStart + 1);
		}
	}
}

void AseLoader::Ase2Obj() //only convert the necessary data
{
	Mesh me;

	int faceNum = 0;
	for (auto var : mesh_face_list)
	{
		faceNum = var.first;
		glm::ivec3 face = mesh_tfacelist[faceNum];

		Vertex v1;
		v1.Position = mesh_vertex_list[var.second.x];
		v1.Normal = mesh_normals[faceNum].vertexNormal[0];
		v1.TextureCoordinate.x = mesh_tvertlist[face.x].x;
		v1.TextureCoordinate.y = mesh_tvertlist[face.x].y;
		me.Vertices.push_back(v1);

		Vertex v2;
		v2.Position = mesh_vertex_list[var.second.y];
		v2.Normal = mesh_normals[faceNum].vertexNormal[1];
		v2.TextureCoordinate.x = mesh_tvertlist[face.y].x;
		v2.TextureCoordinate.y = mesh_tvertlist[face.y].y;
		me.Vertices.push_back(v2);

		Vertex v3;
		v3.Position = mesh_vertex_list[var.second.z];
		v3.Normal = mesh_normals[faceNum].vertexNormal[2];
		v3.TextureCoordinate.x = mesh_tvertlist[face.z].x;
		v3.TextureCoordinate.y = mesh_tvertlist[face.z].y;
		me.Vertices.push_back(v3);
	}



	LoadedMeshes.push_back(me);

	Material ma;
	std::size_t first = 2;
	ma.map_Kd = diffuse_map.bitmap.substr(first, diffuse_map.bitmap.size() - 3);
	if (specular_map.bitmap.size() > 0)
		ma.map_Ks = specular_map.bitmap.substr(first, specular_map.bitmap.size() - 3);
	if (normal_map.bitmap.size() > 0)
		ma.map_bump = normal_map.bitmap.substr(first, normal_map.bitmap.size() - 3);

	LoadedMaterials.push_back(ma);
}

#ifndef SHADER_H
#define SHADER_H

#include "StandardIncludes.h"

class Shader
{
public:
	//Constructor/Destructors
	Shader();
	virtual ~Shader() {};


	//Accessors
	GLuint GetProgramID() { return m_programID; }
	GLuint GetAttrVertices() { return m_attrVertices; }
	GLuint GetAttrColors() { return m_attrColors; }
	GLuint GetAttrNormals() { return m_attrNormals; }
	GLuint GetAttrTangents() { return m_attrTangents; }
	GLuint GetAttrBitangents() { return m_attrBitangents; }
	GLuint GetAttribWVP() { return m_attrWVP; }
	GLuint GetAttrInstanceMatrix() { return m_attrInstanceMatrix; }
	GLuint GetAttrTexCoords() { return m_attrTexCoords; }

	//Methods
	void LoadShaders(const char* _vertexFilePath, const char* _fragmentFilePath);
	void CleanUp();
	void SetTextureSampler(const char* _name, GLuint _texUnit, GLuint _texUnitID, int _value);
	void SetVec3(const char* _name, glm::vec3 _value);
	void SetMat4(const char* _name, glm::mat4 _value);
	void SetFloat(const char* _name, float _value);
	void SetInt(const char* _name, int _value);

private:
	//Methods
	void CreateShaderProgram(const char* _vertexFilePath, const char* _fragmentFilePath);
	GLuint LoadShaderFile(const char* _filePath, GLenum _type);
	void LoadAttributs();
	void EvaluateShader(int infoLength, GLuint _id);


	//Members
	GLuint m_programID;
	GLuint m_attrVertices;
	GLuint m_attrColors;
	GLuint m_attrNormals;
	GLuint m_attrTangents;
	GLuint m_attrBitangents;
	GLuint m_attrInstanceMatrix;
	GLuint m_attrWVP;
	GLuint m_attrTexCoords;
	GLint m_result = GL_FALSE;
	int m_infoLength;

};
#endif


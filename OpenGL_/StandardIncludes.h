#ifndef STANDARD_INCLUDE_H
#define STANDARD_INCLUDE_H

#define GLM_ENABLE_EXPERIMENTAL

//include standard header
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#define M_ASSERT(_cond,_msg) \
	if(!(_cond)){OutputDebugStringA(_msg); glfwTerminate(); std::abort(); }

#endif

//OpenGL / Helper header
#include <GL/glew.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define generic GenericFromFreeTypeLibrary
#include <ft2build.h>
#include FT_FREETYPE_H
#undef generic

#include "Resolution.h"
#include "Singleton.h"

using namespace std;


#endif // !STANDARD_INCLUDE_H


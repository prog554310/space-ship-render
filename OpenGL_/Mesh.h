#ifndef MESH_H
#define MESH_H

#include "StandardIncludes.h"
#include "Texture.h"
#include "OBJ_Loader.h"
#include "AseLoader.h"

class Shader;


class Mesh
{
public:
	//Constructors/Destructors
	Mesh();
	virtual ~Mesh();

	//Accessors
	void SetPosition(glm::vec3 _position) { m_position = _position; }
	glm::vec3 GetPosition() { return m_position; }
	void SetScale(glm::vec3 _scale) { m_scale = _scale; }
	glm::vec3 GetScale() { return m_scale; }
	void SetRotation(glm::vec3 _rotation) { m_rotation = _rotation; }
	glm::vec3 GetRotation() { return m_rotation; }

	void SetColor(glm::vec3 _color) { m_color = _color; }
	glm::vec3 GetColor() { return m_color; }
	void SetLightPosition(glm::vec3 _lightPosition) { m_lightPosition = _lightPosition; }
	void SetLightColor(glm::vec3 _lightColor) { m_lightColor = _lightColor; }
	void SetCameraPosition(glm::vec3 _cameraPosition) { m_cameraPosition = _cameraPosition; }
	void SetSpecularStrength(float _specularStrength) { m_specularStrength = _specularStrength; };
	void SetSpecularColor(glm::vec3 _specularColor) { m_specularColor = _specularColor; };
	//Methods
	void Create(Shader* _shader,string _file,int _instanceCount = 1);
	void CreateAse(Shader* _shader, string _file, int _instanceCount = 1);
	void CleanUp();
	void CalculateTransform();
	void Render(glm::mat4 _pv);
	string RemoveFolder(string _map);

	static vector<Mesh> Lights;
private:
	void SetShaderVariables(glm::mat4 _pv);
	void BindAttributes();
	void CalculateTangents(vector<objl::Vertex> _vertices, objl::Vector3& _tangent, objl::Vector3& _bitangent);
	void CalculateTangentsAse(vector<AseLoader::Vertex> _vertices, glm::vec3& _tangent, glm::vec3& _bitangent);
	string Concat(string _s1, int _index, string _s2);

	Shader* m_shader;
	Texture m_textureDiffuse;
	Texture m_textureSpecular;
	Texture m_textureNormal;
	GLuint m_vertexBuffer;	//GPU buffer pointer
	GLuint m_indexBuffer;	//GPU buffer pointer
	GLuint m_instanceBuffer;

	vector<GLfloat> m_vertexData;
	vector<GLubyte> m_indexData;
	vector<GLfloat> m_instanceData;
	bool m_enableNormalMap;
	int m_elementSize;
	int m_instanceCount;
	bool m_enableInstancing;

	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
	glm::mat4 m_world;

	glm::vec3 m_lightPosition;
	glm::vec3 m_lightColor;
	glm::vec3 m_color;
	glm::vec3 m_specularColor;
	glm::vec3 m_cameraPosition;
	float m_specularStrength;
};

#endif


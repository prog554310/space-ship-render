#pragma once
#ifndef ASE_LOADER_H
#define ASE_LOADER_H
#include "StandardIncludes.h"

class AseLoader //assume one material always have diffuse map, one mesh
{
public:
	struct Vertex
	{
		// Position Vector
		glm::vec3 Position;

		// Normal Vector
		glm::vec3 Normal;

		// Texture Coordinate Vector
		glm::vec2 TextureCoordinate;
	};

	struct Material
	{
		// Material Name
		std::string name;
		// Ambient Color
		glm::vec3 Ka;
		// Diffuse Color
		glm::vec3 Kd;
		// Specular Color
		glm::vec3 Ks;
		// Specular Exponent
		float Ns;
		// Diffuse Texture Map
		std::string map_Kd;
		// Specular Texture Map
		std::string map_Ks;
		// Bump Map
		std::string map_bump;
	};

	struct Mesh
	{
		// Default Constructor
		Mesh()
		{

		}
		// Variable Set Constructor
		Mesh(std::vector<Vertex>& _Vertices, std::vector<unsigned int>& _Indices)
		{
			Vertices = _Vertices;
		}
		// Vertex List
		std::vector<Vertex> Vertices;
	};



	AseLoader();

	~AseLoader();

	std::vector<Mesh> LoadedMeshes;

	std::vector<Material> LoadedMaterials;

	bool LoadFile(std::string Path);

private:
	
	struct Map
	{
		std::string bitmap;
		float uvw_u_offset;
		float uvw_v_offset;
		float uvw_u_tiling;
		float uvw_v_tiling;
	};

	struct FaceNormal
	{
		glm::vec4 vertexNormal[3];
	};

	glm::vec3 scene_ambient_static;
	//*MATERIAL_LIST 
	int material_count;
	//glm::vec3 material_ambient;
	glm::vec3 material_diffuse;
	glm::vec3 material_specular;
	Map diffuse_map;
	Map specular_map;
	Map normal_map;

	//*GEOMOBJECT 
	int num_meshvertex;
	int num_faces;
	std::map<int, glm::vec3> mesh_vertex_list;
	std::map<int, glm::ivec3> mesh_face_list;

	int num_tvertex;
	std::map<int, glm::vec3> mesh_tvertlist;
	int num_tvfaces;
	std::map<int, glm::ivec3> mesh_tfacelist;
	std::map<int, FaceNormal> mesh_normals;
	
	int material_ref;

	void Ase2Obj();
	int* GetInt(const char* _entry,const std::string& _file);
	float* GetFloat(const char* _entry, const std::string& _file);
	std::string* GetString(const char* _entry, const std::string& _file);
	glm::vec3* GetVec3f(const char* _entry, const std::string& _file);
	glm::ivec3* GetVec3i(const char* _entry, const std::string& _file);
	AseLoader::Map* GetMap(const char* _entry, const std::string& _file);
	void FillVec3iList(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::ivec3>& _targetList);
	void FillVec3List(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::vec3>& _targetList);
	void FillFaceList(const char* _entry, const char* _elementEntry, const std::string& _file, std::map<int, glm::ivec3>& _targetList);
	void FillNormalList(const std::string& _file, std::map<int, FaceNormal>& _targetList);
};

#endif



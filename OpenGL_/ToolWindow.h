#pragma once

namespace OpenGL {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ToolWindow
	/// </summary>
	public ref class ToolWindow : public System::Windows::Forms::Form
	{
	public:
		static bool moveLight = true;
		static bool colorByPos = false;
		static bool transform = false;
		static bool spaceScene = false;
		static float specularStrength = 4;
		static float R = 1.0f;
		static float G = 1.0f;
		static float B = 1.0f;
		static bool translate = false;
		static bool rotate = false;
		static bool scale = false;
		static bool resetLightPos = false;

	private: System::Windows::Forms::RadioButton^ radioButton_MoveLight;
	private: System::Windows::Forms::Button^ button_ResetLightPosition;
	private: System::Windows::Forms::TrackBar^ trackBar_SpecularStrength;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label_SpecularStrengthValue;
	private: System::Windows::Forms::Label^ label_R;
	private: System::Windows::Forms::TrackBar^ trackBar_R;
	private: System::Windows::Forms::Label^ label_RValue;
	private: System::Windows::Forms::Label^ label_GValue;
	private: System::Windows::Forms::TrackBar^ trackBar_G;
	private: System::Windows::Forms::Label^ label_G;
	private: System::Windows::Forms::Label^ label_BValue;
	private: System::Windows::Forms::TrackBar^ trackBar_B;
	private: System::Windows::Forms::Label^ label_B;








	private: System::Windows::Forms::Label^ label_SpecularColor;

	public:
		ToolWindow(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ToolWindow()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->radioButton_MoveLight = (gcnew System::Windows::Forms::RadioButton());
			this->button_ResetLightPosition = (gcnew System::Windows::Forms::Button());
			this->trackBar_SpecularStrength = (gcnew System::Windows::Forms::TrackBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label_SpecularStrengthValue = (gcnew System::Windows::Forms::Label());
			this->label_SpecularColor = (gcnew System::Windows::Forms::Label());
			this->label_R = (gcnew System::Windows::Forms::Label());
			this->trackBar_R = (gcnew System::Windows::Forms::TrackBar());
			this->label_RValue = (gcnew System::Windows::Forms::Label());
			this->label_GValue = (gcnew System::Windows::Forms::Label());
			this->trackBar_G = (gcnew System::Windows::Forms::TrackBar());
			this->label_G = (gcnew System::Windows::Forms::Label());
			this->label_BValue = (gcnew System::Windows::Forms::Label());
			this->trackBar_B = (gcnew System::Windows::Forms::TrackBar());
			this->label_B = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_SpecularStrength))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_R))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_G))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_B))->BeginInit();
			this->SuspendLayout();
			// 
			// radioButton_MoveLight
			// 
			this->radioButton_MoveLight->AutoSize = true;
			this->radioButton_MoveLight->Checked = true;
			this->radioButton_MoveLight->Location = System::Drawing::Point(12, 12);
			this->radioButton_MoveLight->Name = L"radioButton_MoveLight";
			this->radioButton_MoveLight->Size = System::Drawing::Size(78, 17);
			this->radioButton_MoveLight->TabIndex = 0;
			this->radioButton_MoveLight->TabStop = true;
			this->radioButton_MoveLight->Text = L"Move Light";
			this->radioButton_MoveLight->UseVisualStyleBackColor = true;
			this->radioButton_MoveLight->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::radioButton_MoveLight_CheckedChanged);
			// 
			// button_ResetLightPosition
			// 
			this->button_ResetLightPosition->Location = System::Drawing::Point(13, 36);
			this->button_ResetLightPosition->Name = L"button_ResetLightPosition";
			this->button_ResetLightPosition->Size = System::Drawing::Size(131, 23);
			this->button_ResetLightPosition->TabIndex = 1;
			this->button_ResetLightPosition->Text = L"Reset Light Position";
			this->button_ResetLightPosition->UseVisualStyleBackColor = true;
			this->button_ResetLightPosition->Click += gcnew System::EventHandler(this, &ToolWindow::button_ResetLightPosition_Click);
			// 
			// trackBar_SpecularStrength
			// 
			this->trackBar_SpecularStrength->Location = System::Drawing::Point(126, 79);
			this->trackBar_SpecularStrength->Maximum = 128;
			this->trackBar_SpecularStrength->Minimum = 1;
			this->trackBar_SpecularStrength->Name = L"trackBar_SpecularStrength";
			this->trackBar_SpecularStrength->Size = System::Drawing::Size(389, 45);
			this->trackBar_SpecularStrength->TabIndex = 2;
			this->trackBar_SpecularStrength->Value = 4;
			this->trackBar_SpecularStrength->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::trackBar_SpecularStrength_ValueChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(28, 79);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(92, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Specular Strength";
			// 
			// label_SpecularStrengthValue
			// 
			this->label_SpecularStrengthValue->AutoSize = true;
			this->label_SpecularStrengthValue->Location = System::Drawing::Point(522, 79);
			this->label_SpecularStrengthValue->Name = L"label_SpecularStrengthValue";
			this->label_SpecularStrengthValue->Size = System::Drawing::Size(13, 13);
			this->label_SpecularStrengthValue->TabIndex = 4;
			this->label_SpecularStrengthValue->Text = L"4";
			// 
			// label_SpecularColor
			// 
			this->label_SpecularColor->AutoSize = true;
			this->label_SpecularColor->Location = System::Drawing::Point(31, 133);
			this->label_SpecularColor->Name = L"label_SpecularColor";
			this->label_SpecularColor->Size = System::Drawing::Size(76, 13);
			this->label_SpecularColor->TabIndex = 5;
			this->label_SpecularColor->Text = L"Specular Color";
			// 
			// label_R
			// 
			this->label_R->AutoSize = true;
			this->label_R->Location = System::Drawing::Point(114, 133);
			this->label_R->Name = L"label_R";
			this->label_R->Size = System::Drawing::Size(15, 13);
			this->label_R->TabIndex = 6;
			this->label_R->Text = L"R";
			// 
			// trackBar_R
			// 
			this->trackBar_R->Location = System::Drawing::Point(135, 130);
			this->trackBar_R->Maximum = 300;
			this->trackBar_R->Name = L"trackBar_R";
			this->trackBar_R->Size = System::Drawing::Size(380, 45);
			this->trackBar_R->TabIndex = 7;
			this->trackBar_R->Value = 100;
			this->trackBar_R->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::trackBar_R_ValueChanged);
			// 
			// label_RValue
			// 
			this->label_RValue->AutoSize = true;
			this->label_RValue->Location = System::Drawing::Point(522, 133);
			this->label_RValue->Name = L"label_RValue";
			this->label_RValue->Size = System::Drawing::Size(28, 13);
			this->label_RValue->TabIndex = 8;
			this->label_RValue->Text = L"1.00";
			// 
			// label_GValue
			// 
			this->label_GValue->AutoSize = true;
			this->label_GValue->Location = System::Drawing::Point(522, 184);
			this->label_GValue->Name = L"label_GValue";
			this->label_GValue->Size = System::Drawing::Size(28, 13);
			this->label_GValue->TabIndex = 11;
			this->label_GValue->Text = L"1.00";
			// 
			// trackBar_G
			// 
			this->trackBar_G->Location = System::Drawing::Point(135, 181);
			this->trackBar_G->Maximum = 300;
			this->trackBar_G->Name = L"trackBar_G";
			this->trackBar_G->Size = System::Drawing::Size(380, 45);
			this->trackBar_G->TabIndex = 10;
			this->trackBar_G->Value = 100;
			this->trackBar_G->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::trackBar_G_ValueChanged);
			// 
			// label_G
			// 
			this->label_G->AutoSize = true;
			this->label_G->Location = System::Drawing::Point(114, 184);
			this->label_G->Name = L"label_G";
			this->label_G->Size = System::Drawing::Size(15, 13);
			this->label_G->TabIndex = 9;
			this->label_G->Text = L"G";
			// 
			// label_BValue
			// 
			this->label_BValue->AutoSize = true;
			this->label_BValue->Location = System::Drawing::Point(522, 235);
			this->label_BValue->Name = L"label_BValue";
			this->label_BValue->Size = System::Drawing::Size(28, 13);
			this->label_BValue->TabIndex = 14;
			this->label_BValue->Text = L"1.00";
			// 
			// trackBar_B
			// 
			this->trackBar_B->Location = System::Drawing::Point(135, 232);
			this->trackBar_B->Maximum = 300;
			this->trackBar_B->Name = L"trackBar_B";
			this->trackBar_B->Size = System::Drawing::Size(380, 45);
			this->trackBar_B->TabIndex = 13;
			this->trackBar_B->Value = 100;
			this->trackBar_B->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::trackBar_B_ValueChanged);
			// 
			// label_B
			// 
			this->label_B->AutoSize = true;
			this->label_B->Location = System::Drawing::Point(114, 235);
			this->label_B->Name = L"label_B";
			this->label_B->Size = System::Drawing::Size(15, 13);
			this->label_B->TabIndex = 12;
			this->label_B->Text = L"R";
			// 
			// ToolWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(622, 316);
			this->Controls->Add(this->label_BValue);
			this->Controls->Add(this->trackBar_B);
			this->Controls->Add(this->label_B);
			this->Controls->Add(this->label_GValue);
			this->Controls->Add(this->trackBar_G);
			this->Controls->Add(this->label_G);
			this->Controls->Add(this->label_RValue);
			this->Controls->Add(this->trackBar_R);
			this->Controls->Add(this->label_R);
			this->Controls->Add(this->label_SpecularColor);
			this->Controls->Add(this->label_SpecularStrengthValue);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->trackBar_SpecularStrength);
			this->Controls->Add(this->button_ResetLightPosition);
			this->Controls->Add(this->radioButton_MoveLight);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"ToolWindow";
			this->Text = L"Tool Window";
			this->TopMost = true;
			this->Load += gcnew System::EventHandler(this, &ToolWindow::ToolWindow_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_SpecularStrength))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_R))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_G))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_B))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ToolWindow_Load(System::Object^ sender, System::EventArgs^ e)
	{

	}
	private: System::Void trackBar_SpecularStrength_ValueChanged(System::Object^ sender, System::EventArgs^ e)
	{
		specularStrength = trackBar_SpecularStrength->Value;
		label_SpecularStrengthValue->Text = specularStrength + "";
	}

	private: System::Void trackBar_R_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		R = trackBar_R->Value / 100.0f;
		label_RValue->Text = R + "";
	}
	private: System::Void trackBar_G_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		G = trackBar_G->Value / 100.0f;
		label_GValue->Text = G + "";
	}
	private: System::Void trackBar_B_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		B = trackBar_B->Value / 100.0f;
		label_BValue->Text = B + "";
	}
	private: System::Void radioButton_MoveLight_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		moveLight = radioButton_MoveLight->Checked;
	}
	private: System::Void button_ResetLightPosition_Click(System::Object^ sender, System::EventArgs^ e) {
		resetLightPos = true;
	}

};
}

#ifndef GAME_CONTROLLER_H
#define GAME_CONTROLLER_H

#include "StandardIncludes.h"
#include "Shader.h"
#include "Mesh.h"
#include "Skybox.h"
#include "Camera.h"

class GameController : public Singleton<GameController>
{
public:
	//Contructor/Destructor
	GameController();
	virtual ~GameController();

	//Methods
	void Initialize();
	void RunGame();
	static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

private:
	Shader m_shaderColor;
	Shader m_shaderDiffuse;
	Shader m_shaderFont;
	Camera m_camera;
	Shader m_shaderSkybox;
	vector<Mesh> m_meshBoxes;
	Skybox m_skybox;
	GLuint m_vao;
	static glm::vec3 lightTarget;
	static double xpos, ypos;
	static string leftButtonStat;
	static string middleButtonStat;
	static int moving;
};
#endif 



